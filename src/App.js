import React from 'react';
import postsFromJSON from './json/posts.json'


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      newPost: { title: '', body: '', tags: '' },
    };
  }

  componentDidMount() {
    this.loadPostsFromJSON();
  }

  loadPostsFromJSON() {
    localStorage.setItem('posts', JSON.stringify(postsFromJSON));
    this.setState({ posts: JSON.parse(localStorage.getItem('posts')) });
  }

  deletePost(id) {
    const articles = [...this.state.posts];
    const updatedArticles = articles.filter(article => article.id !== id);
    this.setState({ posts: updatedArticles });
    localStorage.setItem('posts', JSON.stringify(updatedArticles))
  }

  updateInput(key, value) {
    this.setState({ newPost: { ...this.state.newPost, [key]: value }});
  }

  updateTags(key, value) {
    const tags = value ? value.split(',') : null;
    if (tags) {
      this.setState({ newPost: { ...this.state.newPost, [key]: tags }});
    }
  }

  addPost(e) {
    e.preventDefault();
    const { posts, newPost } = this.state;
    if (!newPost.title) {
      alert('Заполните поле заголовка')
    } else if (!newPost.body) {
      alert('Заполните поле записи')
    } else if (!newPost.tags) {
      alert('Заполните теги')
    } else if (newPost.title && newPost.body && newPost.tags) {
      newPost.id = posts.length + 1;
      localStorage.setItem('posts', JSON.stringify([...posts, newPost]));
      this.setState({
        posts: [...posts, newPost],
        newPost: { title: '', body: '', tags: '' },
      })
    }
  }

  render() {
    const { posts, newPost } = this.state;
    return (
      <section>
      <div id='posts' className='well'>
        {
          posts && posts.map(row => {
            return (
              <article>
                <header>
                    <h3>{row.title}</h3>
                </header>
                <section>
                    <p>{row.body}</p>
                </section>
                <footer>
                    <div className='tags'>
                      {
                        row.tags && row.tags.map((tag, index) => {
                          return (
                          <><button key={index} className='btn btn-xs btn-default'>{tag}</button>&nbsp;</>
                          )
                        })
                      }
                    </div>
                </footer>
                <div className='controls'>
                    <button
                        className='btn btn-danger btn-mini'
                        onClick={()=>this.deletePost(row.id)}>
                          удалить
                    </button>
                </div>
            </article>
            )
        })
      }

      </div>

      <form id='post-add' className='col-lg-4'>
          <div className='form-group'>
              <input
                type='text'
                className='form-control'
                name='title'
                placeholder='заголовок'
                value={newPost.title}
                onChange={e => this.updateInput('title', e.target.value)} />
          </div>
          <div className='form-group'>
              <input
                type='text'
                className='form-control'
                name='body'
                placeholder='запись'
                value={newPost.body}
                onChange={e => this.updateInput('body', e.target.value)} />
          </div>
          <div className='form-group'>
              <input
                type='text'
                className='form-control'
                name='tags'
                placeholder='тег, еще тег'
                value={newPost.tags}
                onChange={e => this.updateTags('tags', e.target.value)} />
          </div>
          <button
            type='submit'
            className='btn btn-primary'
            onClick={(e) => this.addPost(e)}
            >
              Добавить
          </button>
      </form>
  </section>
    )
  }
}

export default App;
